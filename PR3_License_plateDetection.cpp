#include "main.hpp"
#include "license_plate.cpp"
#include <fstream>


int main(void)
{
    cv::Mat imgCrop;
    //std::ofstream outputFile("regox.txt");
    wiringPiSetupGpio();
    pinMode(RED, OUTPUT);
    pinMode(BLUE,OUTPUT);
    pinMode(GREEN,OUTPUT);
    digitalWrite(GREEN, HIGH);
    std::future<int> result = startProgramAsync();
    //Check if the command was successful
   checkAsyncResult(result);

    //Wait for some time for the program to start
    std::this_thread::sleep_for(std::chrono::seconds(3));

    //Establish the TCP connection(client) with the stream (server)
   std::string streamAddress = "tcp://127.0.0.1:8";
    cv::VideoCapture cap = establishConnection(streamAddress);
    cv::Mat img;
    std::string path = "/home/mavcamtime/Desktop/LopuToo/Final_Solution/test.jpg";
    if(!cap.isOpened())
    {
        std::cout << "could not opene the camera" << std::endl;
        digitalWrite(RED, HIGH);
        exit(0);
    }
    //cv::namedWindow("Image",cv::WINDOW_NORMAL);
    cv::CascadeClassifier plateCascade;
    plateCascade.load("./haarcascade_plate_number.xml");//loading the pre-trained model

    //Check if the file was loaded or not
    if(plateCascade.empty())
    {
        std::cout << "XML file not loaded" << std::endl;
        digitalWrite(RED, HIGH);
        return -1;
    }
    std::vector<cv::Rect> plate;//using to store the boudning boxes of rectangles
    double total_time = 0;
    int total_plate_detected = 0 ;
    int frame_count = 0;
    //img = cv::imread(path);
    while(true)
    {
        
        cap.read(img);
        cv::Mat gray;
        cv::cvtColor(img,gray,cv::COLOR_BGR2GRAY);

        //Tuvastas objekti kiirus
        double start_time = static_cast<double>(cv::getTickCount());
        
        plateCascade.detectMultiScale(img,plate,1.1,10); //the first parameter is the image where you want to detect detect 
                                                        //second parameter you use for storing your detected boxes of the inpat Mat array
                                                        //1.1 is the scale factor 
                                                        //minimum neighburs
        double end_time = static_cast<double>(cv::getTickCount());
        double time_per_frame = (end_time - start_time) /cv::getTickFrequency();
        // Convert time to hh:mm:ss:ms format
        int milliseconds = static_cast<int>((time_per_frame - static_cast<int>(time_per_frame)) * 1000);
        int seconds = static_cast<int>(time_per_frame) % 60;
        int minutes = (static_cast<int>(time_per_frame) / 60) % 60;
        int hours = static_cast<int>(time_per_frame) / 3600;
        std::cout <<"\r\n" << std::endl;
        // Print the time
        std::cout << std::setfill('0') << std::setw(2) << hours << ":"
                  << std::setfill('0') << std::setw(2) << minutes << ":"
                  << std::setfill('0') << std::setw(2) << seconds << ":"
                  << std::setfill('0') << std::setw(3) << milliseconds << std::endl;

        total_time += time_per_frame;
        total_plate_detected += plate.size();
        std::cout << "tuvastati "<< plate.size() << "objektid" << std::endl;
        frame_count++;
        



        //we can iterater through all the faces, that we have detected and then
        //draw them one by one                         
        for(int i = 0 ; i < (int)plate.size();i++)
        {
           // imgCrop = img(plate[i]);
	        
            //cv::cvtColor(imgCrop,imgCrop,cv::COLOR_BGR2GRAY);
            //cv::threshold(imgCrop,imgCrop,0,255,cv::THRESH_BINARY|cv::THRESH_OTSU);   


            //std::string recognizeText = readLicensePlate(imgCrop);
            //std::cout << recognizeText  << std::endl;
            //outputFile << recognizeText << std::endl;
            //std::cout << "Plate" << i+1 << ":" << recognizeText << std::endl;
            //imwrite("/plates/" + to_string(i) + ".png",imgCrop);
            //cv::imshow("imgCrop",imgCrop);
            cv::rectangle(img,plate[i].tl(),plate[i].br(),cv::Scalar(255,0,255),7);
            imgCrop = img(plate[i]);
           
        }
        /*if(plate.empty())
        {
            std::cout << "not empty" << std::endl; 
        }*/
        cv::imshow("Image",img);
        std::string licenseText;
        if(!plate.empty())
        {
            licenseText = readLicensePlate(imgCrop);
            std::cout << "the license plate "<<licenseText <<std::endl; 
            if(!licenseText.empty())
            {
                processLicensePlate(licenseText, 10,5); // Prevent repeated reads within 2 seconds
            }
        }
        
        
        //std::cout << "frame" << "\r\n" <<std::endl;
	
        if(cv::waitKey(1) == 'q')
        {
            break;
        }
    }
    double avarage_time_per_frame = total_time/frame_count;
    std::cout << "avarage speed" << avarage_time_per_frame << "sekundi kaadri kohta" << std::endl;

    double avarage_plate_detected = static_cast<double>(total_plate_detected)/ frame_count;
    std::cout << "keskmine tuvastatud objekti arv" << avarage_plate_detected << std::endl;
    digitalWrite(BLUE, HIGH);

    return 0;
}
