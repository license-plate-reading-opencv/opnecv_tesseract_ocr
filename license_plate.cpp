#include "main.hpp"
#include "display.cpp"

/*
*
* @descritpion:std::asyncis a function that creates and runs a new asynchronous task, it takes two arguments:
* -std::launch::asynch- this specifies the task should be launched asynchorously, meanint it will start running immediatly in a seperate thread
* -[](){.....} - this is a lambda function that defines the task to be executed asynchronously
*  in the return std::system(...) within theeee lambda functioon passed to the std::asynch ,  std:;systeeeem is caalled to execute
* a command in the shell. The command to be executed is :"sudo nice -n -20 libcamera-vid -n -t 0 --rotation 180 --width 680  --height 692 --framerate 30 --inline --listen -o tcp://127.0.0.1:8888"
*
* @return : The std::future<int> returned by std::async represents the result of the asynchronous task. In this case it represents the exit status of the command executed by std::system.
*/

std::future<int> startProgramAsync()
{
    return std::async(std::launch::async,[]()
    {
        return std::system("sudo nice -n -20 libcamera-vid -n -t 0 --rotation 180 --width 640  --height 480 --framerate 30 --inline --listen -o tcp://127.0.0.1:8"); //without zoom:
                                                                                                                                                                        //--width 680  --height 692 

    });
}


/*
* @descritpion:
*
* @return:
*/
void checkAsyncResult(std::future<int>& result)
{
    if(result.valid())
	{
		std::cout << "Command started asynchronously " <<std::endl;
	}
	else
	{
		std::cout << "failed to start the command asynchronously " << std::endl;
		exit(0);
	}
}


/*
* @descritpion:
*
* @return:
*/
cv::VideoCapture establishConnection(const std::string &streamAddress)
{
    cv::VideoCapture cap(streamAddress);
    if(!cap.isOpened())
    {
        std::cout << "Could not open the stream service, pleace check the address" << std::endl;
        std::cout << "Exiting programm !" << std::endl;
        digitalWrite(RED, LOW);
        exit(0);
    }
    else
    {
        std::cout << "Stream service is active, the address is correct" << std::endl;
    }
    return cap;
}


cv::Mat imagePreProcessing(const cv::Mat& frame)
{
    cv::Mat grayScale, blur,thresh;
    
    //change the BGR to grayscale
    /*cv::cvtColor(frame,grayScale,cv::COLOR_BGR2GRAY);
    cv::imshow("pre",grayScale);*/
    //blurring image
    cv::GaussianBlur(frame,blur,cv::Size(3,3),5,0);
    //cv::imshow("pre",blur);
    //using thresh to clear up the numbers and to clear the background
    //cv::threshold(blur,thresh,0,255,cv::THRESH_BINARY|cv::THRESH_OTSU); 
    //cv::imshow("pre",thresh);
    cv::waitKey(1);
    //return the preprocessed image
    return thresh;
    
}
/*int levenshteinDistance(const std::string& s1, const std::string& s2) {
    // Initialize a matrix to store the distances
    std::vector<std::vector<int>> dp(s1.size() + 1, std::vector<int>(s2.size() + 1));

    // Initialize the first row and column
    for (int i = 0; i <= s1.size(); ++i) {
        dp[i][0] = i;
    }
    for (int j = 0; j <= s2.size(); ++j) {
        dp[0][j] = j;
    }

    // Fill in the rest of the matrix
    for (int i = 1; i <= s1.size(); ++i) {
        for (int j = 1; j <= s2.size(); ++j) {
            int cost = (s1[i - 1] == s2[j - 1]) ? 0 : 1;
            dp[i][j] = std::min({dp[i - 1][j] + 1, dp[i][j - 1] + 1, dp[i - 1][j - 1] + cost});
        }
    }

    // Return the Levenshtein distance
    return dp[s1.size()][s2.size()];
}*/
// Function to calculate accuracy
/*double calculateAccuracy(const std::string& groundTruthText, const std::string& recognizedText) {
    int distance = levenshteinDistance(groundTruthText, recognizedText);
    int maxLength = std::max(groundTruthText.length(), recognizedText.length());
    return 1.0 - (static_cast<double>(distance) / maxLength);
}*/


std::string readLicensePlate(const cv::Mat & frame)
{
/////////////////Solution 1//////////////////////////////
    std::string outText;
 
    cv::Mat resized_license_plate;
  

    cv::Mat grayscale;
    cv::cvtColor(frame, grayscale, cv::COLOR_BGR2GRAY);
    cv::imshow("grayscale", grayscale);

    cv::Mat blur;
    cv::GaussianBlur(grayscale, blur, cv::Size(7, 7),0);
    cv::imshow("image blur",blur);

    cv::Mat edges;
    cv::Canny(blur, edges, 25, 75,7,true);
    cv::imshow("edge",edges);

    cv::Mat kernel = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3));
    cv::Mat imgDil, imgErod;
    cv::dilate(edges, imgDil, kernel);
    cv::imshow("dil",imgDil);

    cv::erode(imgDil, imgErod, kernel);
    cv::imshow("erod",imgErod);

    cv::Mat thresh;
    //cv::threshold(imgErod, thresh, 0, 255, cv::THRESH_BINARY | cv::THRESH_OTSU);
    cv::adaptiveThreshold(imgDil, thresh, 255, cv::ADAPTIVE_THRESH_GAUSSIAN_C, cv::THRESH_BINARY, 15, 2);


    cv::imshow("Processed Image", thresh);

    tesseract::TessBaseAPI api;
    if(api.Init(NULL,"eng"))
    {
    	std::cout << "Error while initalizint tesseract"<< std::endl;
    	exit(0);
    }
    
    // Set Tesseract parameters
    api.SetVariable("tessedit_char_whitelist", "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789");
   
    api.SetPageSegMode(tesseract::PSM_SINGLE_LINE);
    


   // auto start = std::chrono::steady_clock::now();
    // Perform OCR
    api.SetImage(grayscale.data, grayscale.cols, grayscale.rows, 1, grayscale.step);
    char* ocrText = api.GetUTF8Text();
   // auto end = std::chrono::steady_clock::now();
   //auto CheckDurationAnalyz = std::chrono::duration_cast<std::chrono::milliseconds>(end-start);
   // double accur = calculateAccuracy("441AUI",ocrText);
    // Open the file for writing
    //std::ofstream outFile("data.csv", std::ios::app);

    // Check if the file is open
    /*if (!outFile.is_open()) {
        std::cerr << "Error: Unable to open file for writing\n";
        
    }*/
        // Format the time difference as hh:mm:ss:ms
    /*auto milliseconds = CheckDurationAnalyz.count() % 1000;
    auto seconds = std::chrono::duration_cast<std::chrono::seconds>(CheckDurationAnalyz).count() % 60;
    auto minutes = std::chrono::duration_cast<std::chrono::minutes>(CheckDurationAnalyz).count() % 60;
    auto hours = std::chrono::duration_cast<std::chrono::hours>(CheckDurationAnalyz).count();
    std::ostringstream formattedTime;
    formattedTime << std::setw(2) << std::setfill('0') << hours << ":";
    formattedTime << std::setw(2) << std::setfill('0') << minutes << ":";
    formattedTime << std::setw(2) << std::setfill('0') << seconds << ":";
    formattedTime << std::setw(3) << std::setfill('0') << milliseconds;*/

    //std::string formattedTimeString = formattedTime.str();

    // Write data to the file
    //outFile << "441AUI" << "," <<formattedTimeString << "," << accur << "\n";

    // Close the file
    //outFile.close();
    

    // Filter the OCR result
    std::string filteredText(ocrText);
   
    filteredText.erase(std::remove_if(filteredText.begin(), filteredText.end(),
                                [](unsigned char c) { return !std::isalnum(c); }),
                                                                 filteredText.end());

// Print the filtered result


    // Perform OCR on the blurred image
  api.SetImage(thresh.data, thresh.cols, 
                 thresh.rows, 1, thresh.step);
    //api.SetImage(image);
    char* new_predicted_result_GWT2180 = api.GetUTF8Text();
    // Filter the predicted result
    std::string filter_new_predicted_result_GWT2180;
    for (int i = 0; new_predicted_result_GWT2180[i] != '\0'; ++i) {
        if (isalnum(new_predicted_result_GWT2180[i])) {
            filter_new_predicted_result_GWT2180 += new_predicted_result_GWT2180[i];
        }
    }
    
    // Print the filtered result
    //std::cout << "Filtered OCR Output: " << new_predicted_result_GWT2180 << std::endl;
    std::cout << "Filtered OCR Output: " << filteredText << std::endl;
    // Release resources
    api.End();
    
    //return new_predicted_result_GWT2180;
    return filteredText;
////////////////////////Solution 2/////////////////////
  // Initialize Python interpreter
   /* Py_Initialize();
    import_array();  // Initialize NumPy API
    
    // Add current directory to Python module search path
    PyRun_SimpleString("import sys\nsys.path.append('.')");

    // Load the Python module
    PyObject *pName = PyUnicode_DecodeFSDefault("main");
    PyObject *pModule = PyImport_Import(pName);
    Py_DECREF(pName);
    if (!pModule) {
        std::cerr << "Failed to import Python module" << std::endl;
        exit(0);
    }

    // Get the function from the module
    PyObject *pFunc = PyObject_GetAttrString(pModule, "recognize_text");
    if (!pFunc || !PyCallable_Check(pFunc)) {
        std::cerr << "Function not found or not callable" << std::endl;
        Py_DECREF(pModule);
        Py_XDECREF(pFunc);
        exit(0);
    }


   // cv::Mat preProc = imagePreProcessing(frame);
    cv::Mat gray,thresh;
    cv::cvtColor(frame,gray,cv::COLOR_BGR2RGB);
    cv::Mat blur;
    cv::GaussianBlur(gray,blur,cv::Size(3,3),0);
    //cv::threshold(blur,thresh,0,255,cv::THRESH_BINARY|cv::THRESH_OTSU); 
    // Convert OpenCV image to a NumPy array
    npy_intp dims[3] = { gray.rows, gray.cols, gray.channels() };
    PyObject *pArgs = PyArray_SimpleNewFromData(3, dims, NPY_UINT8, gray.data);
    if (!pArgs) {
        std::cerr << "Failed to create NumPy array from image data" << std::endl;
        Py_DECREF(pModule);
        Py_DECREF(pFunc);
        exit(0);
    }
    //Create a tuple with the NumPy array as the only element
    PyObject *pTuple = PyTuple_New(1);
    if (!pTuple) {
        std::cerr << "Failed to create tuple for function arguments" << std::endl;
        Py_DECREF(pModule);
        Py_DECREF(pFunc);
        Py_DECREF(pArgs);
        exit(0);
    }
    PyTuple_SetItem(pTuple, 0, pArgs); // Set the first element of the tuple to the NumPy array

    auto start = std::chrono::steady_clock::now();
    // Call the Python function with the argument
    PyObject *pResult = PyObject_CallObject(pFunc, pTuple);
    if (!pResult) {
    // Check if an exception occurred
	if (PyErr_Occurred()) {
	// Retrieve the exception information
		PyErr_Print();
	}
        std::cerr << "Function call failed" << std::endl;
        Py_DECREF(pModule);
        Py_DECREF(pFunc);
        Py_DECREF(pArgs);
        exit(0);
    }	
    std::cout << "extracting " <<std::endl;

    // Extract result from Python object (string)
    const char *result = PyUnicode_AsUTF8(pResult);
    auto end = std::chrono::steady_clock::now();
    auto CheckDurationAnalyz = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
    double accuracy = calculateAccuracy("441 AUI",result);
    std::cout << "Recognized text: " << result << std::endl;
     // Open the file for writing
    std::ofstream outFile("data_easy_OCR.csv", std::ios::app);

    // Check if the file is open
    if (!outFile.is_open()) {
        std::cerr << "Error: Unable to open file for writing\n";
        
    }
          // Format the time difference as hh:mm:ss:ms
    auto milliseconds = CheckDurationAnalyz.count() % 1000;
    auto seconds = std::chrono::duration_cast<std::chrono::seconds>(CheckDurationAnalyz).count() % 60;
    auto minutes = std::chrono::duration_cast<std::chrono::minutes>(CheckDurationAnalyz).count() % 60;
    auto hours = std::chrono::duration_cast<std::chrono::hours>(CheckDurationAnalyz).count();
    std::ostringstream formattedTime;
    formattedTime << std::setw(2) << std::setfill('0') << hours << ":";
    formattedTime << std::setw(2) << std::setfill('0') << minutes << ":";
    formattedTime << std::setw(2) << std::setfill('0') << seconds << ":";
    formattedTime << std::setw(3) << std::setfill('0') << milliseconds;

    std::string formattedTimeString = formattedTime.str();

    // Write data to the file
    outFile << "441AUI" << "," <<formattedTimeString << "," << accuracy << "\n";

    // Close the file
    outFile.close();

    // Cleanup
    //Py_DECREF(pModule);
    //Py_DECREF(pFunc);
    //Py_DECREF(pArgs);
    //Py_DECREF(pResult);

    // Finalize Python interpreter
    //Py_Finalize();
    //std::string presult = "";
    return result;*/


}
/*std::map<std::string, std::chrono::system_clock::time_point> plate_timings;
// Function to process license plate recognition with timing and prevention of repeated reads
std::string processLicensePlate(const std::string &plateNumber, int preventRepeatSeconds) 
{

   
   
    std::chrono::system_clock::time_point start_time;
    // Check if the license plate has been read within the last preventRepeatSeconds seconds
    if (!plateNumber.empty()) 
    {*/
        /*auto last_time = plate_timings[plateNumber];
        auto current_time = std::chrono::system_clock::now();
        auto duration = current_time - last_time;
        if (duration < std::chrono::seconds(preventRepeatSeconds)) {
            std::cout << "License plate " << plateNumber << " discarded. Already read within the last "
                      << preventRepeatSeconds << " seconds." << std::endl;
            return "";
        }*/
       /* auto it = plate_timings.find(plateNumber);
        if(it == plate_timings.end()|| (plate_timings[plateNumber] - it->second) >= std::chrono::seconds(preventRepeatSeconds))
        {
            start_time = std::chrono::system_clock::now();
            plate_timings[plateNumber] = start_time;
            std::cout << "Timing started for key '" << plateNumber << "' at ";
            std::time_t start_time_c = std::chrono::system_clock::to_time_t(start_time);
            std::cout << std::put_time(std::localtime(&start_time_c), "%T") << std::endl;
        }
        else
        {
            // Stop timing for the key and calculate the time difference
            auto end_time = std::chrono::system_clock::now();
            auto duration = end_time - plate_timings[plateNumber];
            auto hours = std::chrono::duration_cast<std::chrono::hours>(duration).count();
            auto minutes = std::chrono::duration_cast<std::chrono::minutes>(duration).count() % 60;
            auto seconds = std::chrono::duration_cast<std::chrono::seconds>(duration).count() % 60;
            auto milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count() % 1000;
            std::cout << "Timing stopped for key '" << plateNumber << "' at ";
            std::time_t end_time_c = std::chrono::system_clock::to_time_t(end_time);
            std::cout << std::put_time(std::localtime(&end_time_c), "%T") << std::endl;
            std::cout << "Time difference: " 
                    << std::setw(2) << std::setfill('0') << hours << ":"
                    << std::setw(2) << std::setfill('0') << minutes << ":"
                    << std::setw(2) << std::setfill('0') << seconds << ":"
                    << std::setw(3) << std::setfill('0') << milliseconds << std::endl;
            // Remove the key from the map to stop timing it
            plate_timings.erase(plateNumber);
        } 
    } 
    
    //return "";
}*/

// Define a map to store the start time for each license plate
/*std::map<std::string, std::chrono::system_clock::time_point> plate_timings;

// Function to process license plate recognition with timing and prevention of repeated reads
std::string processLicensePlate(const std::string &plateNumber, int preventRepeatSeconds) {
    std::chrono::system_clock::time_point start_time;

    // Get the current time
    auto current_time = std::chrono::system_clock::now();

    // Check if the license plate has been read within the last preventRepeatSeconds seconds
    auto it = plate_timings.find(plateNumber);
    if (it == plate_timings.end()*/ /*|| (current_time - it->second) >= std::chrono::seconds(preventRepeatSeconds)) {*/
    /*    start_time = current_time;
        plate_timings[plateNumber] = start_time;
        std::cout << "Timing started for key '" << plateNumber << "' at ";
        std::time_t start_time_c = std::chrono::system_clock::to_time_t(start_time);
        std::cout << std::put_time(std::localtime(&start_time_c), "%T") << std::endl;
    } else {
        // Stop timing for the key and calculate the time difference
        auto end_time = current_time;
        auto duration = end_time - plate_timings[plateNumber];
        auto hours = std::chrono::duration_cast<std::chrono::hours>(duration).count();
        auto minutes = std::chrono::duration_cast<std::chrono::minutes>(duration).count() % 60;
        auto seconds = std::chrono::duration_cast<std::chrono::seconds>(duration).count() % 60;
        auto milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count() % 1000;
        std::cout << "Timing stopped for key '" << plateNumber << "' at ";
        std::time_t end_time_c = std::chrono::system_clock::to_time_t(end_time);
        std::cout << std::put_time(std::localtime(&end_time_c), "%T") << std::endl;
        std::cout << "Time difference: " 
                    << std::setw(2) << std::setfill('0') << hours << ":"
                    << std::setw(2) << std::setfill('0') << minutes << ":"
                    << std::setw(2) << std::setfill('0') << seconds << ":"
                    << std::setw(3) << std::setfill('0') << milliseconds << std::endl;
        // Remove the key from the map to stop timing it
        plate_timings.erase(plateNumber);
    } 
    
    return ""; // Modify this to return the recognized text
}*/
// Define a struct to store the start time and frame count for each license plate
struct PlateInfo {
    std::chrono::system_clock::time_point start_time;
    std::chrono::system_clock::time_point check_frame_time;
    int frame_count;
};

// Define an unordered_map to store plate number and associated PlateInfo
std::unordered_map<std::string, PlateInfo> plate_info;

// Function to process license plate recognition with timing and prevention of repeated reads
std::string processLicensePlate(const std::string &plateNumber, int preventRepeatSeconds, int maxFramesToSkip) {
    // Get the current time
    auto current_time = std::chrono::system_clock::now();
    

    // Check if the license plate has been read within the last preventRepeatSeconds seconds
    if(!plateNumber.empty())
    {
    	digitalWrite(BLUE, HIGH);
        auto it = plate_info.find(plateNumber);
        if (it == plate_info.end()) {
            // If the plate is not in the map or the time difference exceeds preventRepeatSeconds, process the plate
            plate_info[plateNumber] = {current_time,current_time, 0}; // Initialize frame count to 0
            std::cout << "Timing started for key '" << plateNumber << "' at ";
            std::time_t start_time_c = std::chrono::system_clock::to_time_t(current_time);
            std::cout << std::put_time(std::localtime(&start_time_c), "%T") << std::endl;

        } else {
            // If the plate has been recently read, check the frame count
            int frameCount = it->second.frame_count;

            if (frameCount <= maxFramesToSkip && (current_time - it->second.check_frame_time) > std::chrono::seconds(preventRepeatSeconds)) {
                // If the maximum frames to skip has been reached, process the plate
                auto end_time = current_time;
                auto duration = end_time - plate_info[plateNumber].start_time;
                std::chrono::milliseconds duration_ms = std::chrono::duration_cast<std::chrono::milliseconds>(duration);
                auto hours = std::chrono::duration_cast<std::chrono::hours>(duration).count();
                auto minutes = std::chrono::duration_cast<std::chrono::minutes>(duration).count() % 60;
                auto seconds = std::chrono::duration_cast<std::chrono::seconds>(duration).count() % 60;
                auto milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count() % 1000;
                std::cout << "Timing stopped for key '" << plateNumber << "' at ";
                std::time_t end_time_c = std::chrono::system_clock::to_time_t(end_time);
                std::cout << std::put_time(std::localtime(&end_time_c), "%T") << std::endl;
                std::cout << "Time difference: " 
                        << std::setw(2) << std::setfill('0') << hours << ":"
                        << std::setw(2) << std::setfill('0') << minutes << ":"
                        << std::setw(2) << std::setfill('0') << seconds << ":"
                        << std::setw(3) << std::setfill('0') << milliseconds << std::endl;
                //std::cout << duration << std::endl;
                //save the data to csv file
                saveToCSV(plateNumber, duration_ms,"plate_info.csv");
                
                
                // Remove the key from the map to stop timing it
                plate_info.erase(plateNumber);
            }else if(frameCount > maxFramesToSkip) 
            {
                discardingPlate("Discarding plate",plateNumber);
                plate_info.erase(plateNumber);
            }
            else {
                // Increment the frame count and skip processing the plate
                plate_info[plateNumber].frame_count++;
                plate_info[plateNumber].check_frame_time = current_time;
                std::cout << "Skipping processing for key '" << plateNumber << "'. Frame count: " << frameCount << std::endl;
                return ""; // Return empty string to indicate skipped processing
            }
        } 
    }    

    
    return ""; // Modify this to return the recognized text
}
void saveToCSV(const std::string& plateNumber, const std::chrono::milliseconds& timeDifference, const std::string& filename) {
    std::ofstream file(filename, std::ios::app); // Open file in append mode

    if (!file.is_open()) {
        std::cerr << "Error: Unable to open file " << filename << " for writing" << std::endl;
        return;
    }

    // Format the time difference as hh:mm:ss:ms
    auto milliseconds = timeDifference.count() % 1000;
    auto seconds = std::chrono::duration_cast<std::chrono::seconds>(timeDifference).count() % 60;
    auto minutes = std::chrono::duration_cast<std::chrono::minutes>(timeDifference).count() % 60;
    auto hours = std::chrono::duration_cast<std::chrono::hours>(timeDifference).count();
    std::ostringstream formattedTime;
    formattedTime << std::setw(2) << std::setfill('0') << hours << ":";
    formattedTime << std::setw(2) << std::setfill('0') << minutes << ":";
    formattedTime << std::setw(2) << std::setfill('0') << seconds << ":";
    formattedTime << std::setw(3) << std::setfill('0') << milliseconds;

    std::string formattedTimeString = formattedTime.str();
    writeLicenseAndTime(plateNumber,formattedTimeString);

    // Write plate number, start time, and time difference to CSV file
    //file << plateNumber << "," << hours << ":" << minutes << ":" << seconds << ":" << milliseconds << std::endl;
    
    // Write plate number, start time, and time difference to CSV file
    file << plateNumber << "," << formattedTimeString <<  std::endl;

    // Close the file
    file.close();
}

// // Function to format milliseconds to hh:mm:ss:ms format
// std::string formatTime(std::chrono::milliseconds ms) {
//     using namespace std::chrono;
//     auto hours = duration_cast<hours>(ms);
//     ms -= duration_cast<milliseconds>(hours);
//     auto minutes = duration_cast<minutes>(ms);
//     ms -= duration_cast<milliseconds>(minutes);
//     auto seconds = duration_cast<seconds>(ms);
//     ms -= duration_cast<milliseconds>(seconds);

//     std::ostringstream oss;
//     oss << std::setw(2) << std::setfill('0') << hours.count() << ":"
//         << std::setw(2) << std::setfill('0') << minutes.count() << ":"
//         << std::setw(2) << std::setfill('0') << seconds.count() << ":"
//         << std::setw(3) << std::setfill('0') << ms.count();
//     return oss.str();
// }
