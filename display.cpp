#include "main.hpp"

void discardingPlate(const char* message,const std::string &plateNumber)
{
    int lcd;
    wiringPiSetup();
    lcd = lcdInit (2, 16, 4, LCD_RS, LCD_E, LCD_D4, LCD_D5, LCD_D6, LCD_D7, 0, 0, 0, 0);
    lcdClear(lcd);
    lcdPosition(lcd,0,0);
    lcdPuts(lcd,message);
    
}
void writeLicenseAndTime(const std::string &plateNumber,const std::string& time)
{
    int lcd;
    wiringPiSetup();
    lcd = lcdInit (2, 16, 4, LCD_RS, LCD_E, LCD_D4, LCD_D5, LCD_D6, LCD_D7, 0, 0, 0, 0);
    lcdClear(lcd);
    lcdPosition(lcd,0,0);
    lcdPuts(lcd,plateNumber.c_str());
    lcdPosition(lcd,0,1);
    //std::string formattedTimeString = formattedTime.str();
    lcdPuts(lcd,time.c_str());
    
}



