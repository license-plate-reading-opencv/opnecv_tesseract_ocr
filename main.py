import easyocr
import json
import sys

def recognize_text(image_path):
    reader = easyocr.Reader(['en'],gpu = False)
    result = reader.readtext(image_path)
     # Join the recognized text from each element in the result list
    recognize_text = ""
    for out in result:
        text_bbox, text, text_score = out;
        recognize_text += text 
        print(text,text_score);
    #print(recognize_text)
    return recognize_text;

'''if __name__ == "__main__":
    
    if len(sys.argv) != 2:
        print("Usage:", sys.argv[0], "<image_path>")
        sys.exit(1)
    image_path = sys.argv[1]
    recognized_text = recognize_text(image_path)
    print(recognized_text)'''

