#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/objdetect.hpp>//this will let us work with haarscascade model
#include <future>
#include <chrono>
#include <thread>
#include <tesseract/baseapi.h>
#include <leptonica/allheaders.h>
#include <string.h>
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <map>
#include <iomanip>
#include <fstream>
#include <vector>
#include <wiringPi.h>
#include <lcd.h>

#define LCD_RS  25          //Register Pin
#define LCD_E   24          //Enable Pin
#define LCD_D4  23          //Data pin 4 
#define LCD_D5  22          //Data pin 5
#define LCD_D6  21          //Data pin 6
#define LCD_D7  14          //Data pin 7

#define BLUE	12		
#define RED 	21
#define GREEN	20 	


std::future<int> startProgramAsync();
void checkAsyncResult(std::future<int>& result);
cv::VideoCapture establishConnection(const std::string &streamAddress);
std::string readLicensePlate(const std::string & frame);
cv::Mat imagePreProcessing(const cv::Mat& frame );
std::string processLicensePlate(const std::string &plateNumber, int preventRepeatSeconds, int maxFramesToSkip);
void saveToCSV(const std::string& plateNumber, const std::chrono::milliseconds& timeDifference, const std::string& filename); 
std::string formatTime(std::chrono::milliseconds ms);
